package cuisinne;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConversionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    // liste de couples (nom de l'unité, nombre de grammes dans cette unité)
	String[][] units= {{"gramme", "1"},{"kilogramme", "1000"},{"pincée", "0.5"},{"litre", "846"},{"cuillère à café", "5"},{"cuillère à soupe", "15"}, {"tasse",  "170"}};
//	double[] gramsPerUnit = {1, 1000, 0.5, 846, 5, 15, 170};
//	
    public ConversionServlet() {
        super();

    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setAttribute("units", units);
		req.getRequestDispatcher("conversion.jsp").forward(req, resp);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		double value = Double.parseDouble(req.getParameter("number")) ;
		double unitCoef = Double.parseDouble(req.getParameter("unit")); // c'est le nombre de grammes dans l'unité demandée par l'utilisateur
		ArrayList<Double> results = new ArrayList<Double>();
		for (String[] u: units) {
			results.add(value * unitCoef / Double.parseDouble(u[1]));
		}
		req.setAttribute("number", value);
		req.setAttribute("results", results);
		doGet(req, resp);
	}
}
