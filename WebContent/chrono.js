var hour=0
var min=0
var sec=0
var countdownTimer

function start() {
	clearInterval(countdownTimer)
    hour = document.getElementById("heure").value;
    min = document.getElementById("minute").value;
    sec = document.getElementById("seconde").value;
    if (hour==0 && min==0 && sec==0) document.getElementById("minuteur").innerHTML = "Veuillez indiquer un temps !";
    else{
	    timer();
	    countdownTimer = setInterval('timer()', 1000);
    }
    
}

function reset(){
	clearInterval(countdownTimer)
    document.getElementById("minuteur").innerHTML = "Minuteur arrêté : " + hour + "H " + min + "M " + sec + "S";
	hour=0
	min=0
	sec=0
    hour = document.getElementById("heure").value = 0;
    min = document.getElementById("minute").value = 0;
    sec = document.getElementById("seconde").value = 0;
	
}

function timer() {    
    document.getElementById("minuteur").innerHTML = "Temps restant : " + hour + "H " + min + "M " + sec + "S";
    console.log(sec)
    sec--;
    if (sec < 0) {
        min--;
        sec = 59;
        if (min < 0) {
            hour--;
            min = 59;
            if (hour < 0){
                clearInterval(countdownTimer);
            
                document.getElementById("minuteur").innerHTML = "Temps écoulé !";
            }
        } 
    }
}